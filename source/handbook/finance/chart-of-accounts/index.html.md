--- 
layout: markdown_page
title: "Chart of Accounts Policy"
---

## Chart of Accounts Policy

### Scope
This policy establishes GitLab’s guidelines regarding the structure, responsibilities and requirements underlying the [chart of accounts (COA).](https://www.investopedia.com/terms/c/chart-accounts.asp)

### Purpose
This policy establishes formal responsibilities and accountabilities for how GitLab handles requests for new, modified or closed data elements on the COA. The Controller is responsible for all aspects of financial accounting and reporting, and governs the COA.  All requests for new or modified (including closure/deactivation) COA segments, hierarchies, and configuration attributes are subject to approval by the Finance team.

### Changes to the COA
All requests for new or modified accounts must be submitted to the Accounting Manager for review and approval through a request using the Finance issue tracker.

There are other stakeholders associated with the COA that may influence certain business decisions or financial system configurations. The Controller and Accounting Manager will include selected stakeholders in the related procedures and processes when and if appropriate. Potential stakeholders include, but may not be limited to:
- Financial Planning and Analysis
- Data & Analytics
- Other departments who have shared functionalities within the financial system

The general ledger attributes subject to this policy will be defined by the Controller based upon factors including but not limited to:

- Creating and maintaining consistency for the structure of accounts
- Standard accounting policies and practices
- Regulatory compliance requirements and reporting needs
- Financial and operational reporting needs and requirements
- External accounting and financial reporting requirements

Once an amendment to the COA has been approved, the Accounting Manager will ensure the necessary changes are implemented by updating and then closing the issue.


