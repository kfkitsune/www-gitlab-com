---
layout: markdown_page
title: "Gartner Magic Quadrant for Application Release Orchestration 2018"
---

## GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018
This page represents how Gartner views our application release orchestration capabilities in relation to the larger market and how we're working with that information to build a better product. It also provides Gartner with ongoing context for how our product is developing in relation to how they see the market.

More to come here.
